<?php

namespace ingenstream\helper;


use stdClass;

/**
 * 分页帮助类
 *
 * @author Mr.April
 * @since  1.0
 */
class Page
{
    /**
     * 获取分页参数
     *
     * @param array|object $input
     * @param bool         $isPage
     *
     * @return int[]
     */
    public static function getPageValue(array|object $input, bool $isPage = true): array
    {
        $page = $limit = 0;
        if ($isPage) {
            $input = is_array($input) ? (object)$input : ($input instanceof stdClass || is_object($input) ? $input : (object)[]);
            $page  = $input->page ?? 0;
            $limit = $input->limit ?? 0;
        }
        $limitMax     = 1000;
        $defaultLimit = 10;
        if ($limit > $limitMax) {
            $limit = $limitMax;
        }
        return [(int)$page, (int)$limit, (int)$defaultLimit];
    }
}
