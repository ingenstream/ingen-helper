<?php
namespace ingenstream\exception;

use Exception;

class HelperException extends Exception
{
    public function __construct($message, $replace = [], $code = 0, Exception $previous = null)
    {
        if (is_array($message)) {
            $errInfo = $message;
            $message = $errInfo[1] ?? '未知错误';
            if ($code === 0) {
                $code = $errInfo[0] ?? 400;
            }
        }
        parent::__construct($message, $code, $previous);
    }
}
