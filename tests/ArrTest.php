<?php

namespace Tests;

use ingen\helper\Arr;

class ArrTest extends TestCase
{
    public function testAdd()
    {
        $array = Arr::add(['name' => 'ingenueHelper'], 'price', 100);
        $this->assertSame(['name' => 'ingenueHelper', 'price' => 100], $array);
    }
}
